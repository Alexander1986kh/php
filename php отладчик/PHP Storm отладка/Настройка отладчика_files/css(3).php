body,
body.content {
	font:    13px Tahoma,Calibri,Verdana,Geneva,sans-serif;
	color: #222;
	/* Remove the background color to make it transparent */
	background-color: #fff;
	/*background-color:#f3f7f9;*/
	margin:0px;
	padding:8px;
}

body.forum {
	font:    13px Verdana,Arial,Tahoma,Calibri,Geneva,sans-serif;
}

html {
	/* #3658: [IE6] Editor document has horizontal scrollbar on long lines
	To prevent this misbehavior, we show the scrollbar always */
	_overflow-y: scroll;
	cursor:text;
}

img:-moz-broken {
	-moz-force-broken-image-icon : 1;
	width : 24px;
	height : 24px;
}

img, input, textarea {
	cursor: default;
}

img.previewthumb {
	max-width:150px;
	max-height:150px;
	height:auto !important;
	width:auto !important;
	width:150px;
	height:150px;
	margin:1px;
}

img.previewthumb:hover {
	opacity:.5;
	-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
	filter: alpha(opacity=50);
	cursor: pointer;
}

img.size_thumbnail {
	max-height:150px;
	max-width:150px;
}

img.size_medium {
	max-height:300px;
	max-width:300px;
}

img.size_large {
	max-height:1024px;
	max-width:1024px;
}

img.size_fullsize {
	max-width:100%;
	max-height:100%;
	overflow:hidden;
}

img.align_left {
	float:left; /* dont use stylevar for this */
	margin: 1em;
	margin-left: 0;
}

img.align_center {
	margin: 0 auto;
	display:block;
}

img.align_right {
	float:right; /* dont use stylevar for this */
	margin: 1em;
	margin-right: 0;
}

/* Table tag */
table {
	width: auto;
}
table.wysiwyg_dashes {
	border-collapse: collapse;
}

td.wysiwyg_dashes_td {
	border: 1px dotted rgb(153, 153, 153);
	padding: 3px;
}

td.cms_table_grid_td,
td.wysiwyg_cms_table_grid_td {
	border:  solid ;
}

table.cms_table_outer_border,
table.wysiwyg_cms_table_outer_border {
	border-collapse: collapse;
	border:  solid ;
}

td.cms_table_td,
td.cms_table_outer_border_td {
	border:none;
}

/* paragraph tag */
p {
	margin: 0px;
	padding: 0px;
}

/* preview break tag */
hr.previewbreak {
    background-color:red !important;
    border:medium none !important;
    color:red !important;
    height:6px !important;
}

/* pagebreak tag */
h3.wysiwyg_pagebreak {
	border: 1px dashed #cccccc;
	border-top: 3px double black;
	padding: 4px;
}

ol.upper-roman > li {
	list-style:upper-roman outside;
}

ol.lower-roman > li {
	list-style:lower-roman outside;
}

ol.upper-alpha > li {
	list-style:upper-alpha outside;
}

ol.lower-alpha > li {
	list-style:lower-alpha outside;
}